import io.qameta.allure.Description;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.task2.*;

import static core.DriverBase.getDriver;

public class Task2Test extends TestBase {
    private Pages pages;

    private class Pages{
        YandexHomePage yandexHomePage = new YandexHomePage(getDriver());
        MarketHomePage marketHomePage = new MarketHomePage(getDriver());
        ElectronicsPage electronicsPage = new ElectronicsPage(getDriver());
        ElectronicsSubcategoryPage electronicsSubcategoryPage = new ElectronicsSubcategoryPage(getDriver());
        ProductPage productPage = new ProductPage(getDriver());
    }

    @Test(priority = 0)
    @Description("Тесты")
    public void setup(){
        pages = new Pages();//инициализация страниц
        getDriver().get("https://www.yandex.ru");//Переход на страницу яндекса
        pages.yandexHomePage.goMarket();//Переходим в маркет
    }

    /**
     * Проверяем продукт с определенным типом, произваодителем, диапазоном цен исравниваем его названия на странице продукта и в списке продуктов
     * @param productType Тип продукта
     * @param maker производитель
     * @param priceFrom цена от
     * @param priceTo цена до
     */
    @Test(dataProvider = "productData",priority = 1)
    private void productTest(String productType, String maker, String priceFrom, String priceTo){
        pages.marketHomePage.goElectronics();
        pages.electronicsPage.chooseProductType(productType);
        pages.electronicsSubcategoryPage.chooseMaker(maker);
        pages.electronicsSubcategoryPage.choosePrices(priceFrom,priceTo);
        String firstProductName = pages.electronicsSubcategoryPage.firstProduct();
        String choosedProductName = pages.productPage.productName();
        Assert.assertEquals(firstProductName, choosedProductName);
    }

    @DataProvider(name = "productData")
    public static Object[][] productDatas(){
            return new Object[][]{
                     {"Наушники и Bluetooth-гарнитуры","Beats","17000","25000"},
                     {"Мобильные телефоны","Samsung","40000",""}
        };
    }
}

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.task3.AlphaBankPageObjects;
import pageobjects.task3.GooglePageObjects;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import static core.DriverBase.getDriver;

public class Task3Test extends TestBase {
    private Pages pages;

    private class Pages{
        AlphaBankPageObjects alphaBankPageObjects = new AlphaBankPageObjects(getDriver());
        GooglePageObjects googlePageObjects = new GooglePageObjects(getDriver());
    }

    @Test
    public void setup(){
        pages = new Pages();
        getDriver().get("https://www.google.com/");//Переход в поисковую систему гугла
        pages.googlePageObjects.search("Альфа банк");//Ищем альфа банк
        String vacanciesURL = pages.alphaBankPageObjects.goVacanciesPage();//Переход в раздел "Вакансии"
        Assert.assertTrue(vacanciesURL.contains("job.alfabank.ru"));//Проверить,что перешли на job.alfabank.ru
        pages.alphaBankPageObjects.goAboutWorkingInBank();
        String text = pages.alphaBankPageObjects.messageInfo();
        fileWriter(createFile(),text);
    }

    /**
     * @return Файл c определенными правилами названия
     */
    private File createFile(){
        try {
            Capabilities cap = ((RemoteWebDriver) getDriver()).getCapabilities();
            String browserName = cap.getBrowserName().toLowerCase();
            Date currentDate = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-YYYY.HH-mm-ss");
            String dateStr = simpleDateFormat.format(currentDate);
            String fileName = dateStr + "." + browserName + ".google";
            File file = new File(System.getProperty("user.dir") + "\\" + fileName);
            System.out.println(file.getAbsolutePath());
            return file;
        }
        catch (Exception e){
            e.getMessage();
        }
        return null;
    }

    /**
     * Записывает в вайл текст
     * @param file файл, в который нужно записать текст
     * @param text текст
     */
    private void fileWriter(File file,String text){
        try(FileWriter writer = new FileWriter(file,false)){
            writer.write(text);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
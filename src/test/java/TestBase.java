import core.DriverBase;
import listeners.AllureScreenShooter;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import java.util.concurrent.TimeUnit;

import static core.DriverBase.getDriver;

@Listeners(AllureScreenShooter.class)
public class TestBase {

    @BeforeTest(alwaysRun = true)
    public void instantiateDriverObject() {
        DriverBase.instantiateDriverObject();
        String sessionId = ((RemoteWebDriver) getDriver()).getSessionId().toString();
        getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        getDriver().manage().deleteAllCookies();
        getDriver().manage().window().maximize();
    }

    @AfterTest(alwaysRun = true)
    public void closeDriverObjects() {
        DriverBase.closeDriverObjects();
    }
}

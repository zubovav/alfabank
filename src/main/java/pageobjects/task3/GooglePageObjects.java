package pageobjects.task3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.Page;

public class GooglePageObjects extends Page {
    @FindBy(id = "lst-ib")
    private WebElement searchArea;
    @FindBy(xpath = ".//input[@jsaction='sf.chk']")
    private WebElement searchButton;
    @FindBy(xpath = ".//div[@id='rso']/div[1]/div/div/div/div/h3/a")
    private WebElement searchResult;

    /**
     * Поиск и переход к тому, что ищем
     * @param text то, что ищем
     */
    public void search(String text){
        this.searchArea.sendKeys(text);
        this.searchArea.submit();
        this.searchResult.click();
    }

    public GooglePageObjects(WebDriver driver) {
        super(driver);
    }
}

package pageobjects.task3;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.Page;

import static core.DriverBase.getDriver;

public class AlphaBankPageObjects extends Page {
    @FindBy(xpath = ".//a[@href='http://job.alfabank.ru/']")
    private WebElement vacancies;
    @FindBy(xpath = ".//span/a/span[text()='О работе в банке']")
    private WebElement aboutWorkingInBank;
    @FindBy(xpath = ".//div[@class='message']")
    private WebElement message;
    @FindBy(xpath = ".//div[@class='info']")
    private WebElement info;

    /**
     * Переход на страницу вакансии
     * @return url страницы вакансии
     */
    public String goVacanciesPage(){
        this.vacancies.click();
        return getDriver().getCurrentUrl();
    }

    /**
     * Переход в раздел о работе в банке
     */
    public void goAboutWorkingInBank(){
        this.aboutWorkingInBank.click();
    }

    /**
     * @return Текст внизу страницы
     */
    public String messageInfo(){
        String messInfo = this.message.getText() + "\n" + this.info.getText();
        return messInfo;
    }

    public AlphaBankPageObjects(WebDriver driver) {
        super(driver);
    }
}

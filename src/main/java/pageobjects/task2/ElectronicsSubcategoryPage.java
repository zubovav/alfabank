package pageobjects.task2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import pageobjects.Page;

import java.util.List;

public class ElectronicsSubcategoryPage extends Page {
    @FindAll(@FindBy(xpath = ".//fieldset[legend[text()='Производитель']]/ul/li/div/a/label/div/span"))
    private List<WebElement> makers;//Список производителей
    @FindAll(@FindBy(xpath = ".//fieldset[legend[text()='Цена, \u20BD']]/div/ul/li/p/input"))
    private List<WebElement> prices;//Цены
    @FindBy(xpath = ".//div/div[1]/div/div[@class='n-snippet-cell2__title']")
    private WebElement firstElement;//Первый продукт из списка

    /**
     * Выбираем производителя
     * @param maker Производитель
     * @return
     */
    public Page chooseMaker(String maker){
        for (WebElement aMakersList : this.makers) {
            if (aMakersList.getText().equals(maker)) {
                System.out.println(aMakersList.getText());
                aMakersList.click();
                return this;
            }
        }
        return this;
    }

    /**
     * Укахываем диапазон цен
     * @param from цена от
     * @param to цена до
     * @return
     */
    public Page choosePrices(String from, String to){
        this.prices.get(0).sendKeys(from);
        this.prices.get(1).sendKeys(to);
        this.driver.navigate().refresh();
        return this;
    }

    /**
     * Переходим к первому продутку из списка
     * @return Названия первого продуктаиз спика
     */
    public String firstProduct(){
        String firstElementTitle = this.firstElement.getText();
        this.firstElement.click();
        return firstElementTitle;
    }

    public ElectronicsSubcategoryPage(WebDriver driver) {
        super(driver);
    }
}

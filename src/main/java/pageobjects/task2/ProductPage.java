package pageobjects.task2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.Page;

public class ProductPage extends Page {
    @FindBy(xpath = ".//div[@class='n-product-summary__headline']/div/div/div/div/h1")
    private WebElement product;//Название продукта

    /**
     * @return название продукта
     */
    public String productName(){
        return this.product.getText();
    }

    public ProductPage(WebDriver driver) {
        super(driver);
    }
}

package pageobjects.task2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.Page;

public class MarketHomePage extends Page {
    @FindBy(xpath = ".//a[@class='link topmenu__link' and text()='Электроника']")
    private WebElement electronics;// Раздел электроника

    /**
     * Переход в раздел электроника
     * @return
     */
    public Page goElectronics(){
        this.electronics.click();
        return this;
    }

    public MarketHomePage(WebDriver driver) {
        super(driver);
    }
}

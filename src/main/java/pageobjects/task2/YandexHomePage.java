package pageobjects.task2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.Page;

public class YandexHomePage extends Page {
    @FindBy(xpath = ".//a[@data-id='market']")
    private WebElement marketLink;//Маркет

    /**
     * Переход в Маркет
     * @return
     */
    public Page goMarket(){
        this.marketLink.click();
        return this;
    }



    public YandexHomePage(WebDriver driver) {
        super(driver);
    }
}

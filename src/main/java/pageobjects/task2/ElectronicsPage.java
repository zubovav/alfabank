package pageobjects.task2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import pageobjects.Page;
import java.util.List;

public class ElectronicsPage extends Page {
    @FindAll(@FindBy(xpath = ".//a[@class='link catalog-menu__list-item metrika i-bem metrika_js_inited']"))
    private List<WebElement> menuList;//список с типами продуктов

    /**
     * Переходим на страницу с определенным типом продукта
     * @param productType Тип продукта : Мобильные телефоны и т.д.
     * @return
     */
    public Page chooseProductType(String productType){
        for (WebElement aMenuList : this.menuList) {
            if (aMenuList.getText().equals(productType)) {
                System.out.println(aMenuList.getText());
                aMenuList.click();
                return this;
            }
        }
        return this;
    }

    public ElectronicsPage(WebDriver driver) {
        super(driver);
    }
}

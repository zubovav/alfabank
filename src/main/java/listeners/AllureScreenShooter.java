package listeners;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.reporters.ExitCodeListener;

import static core.DriverBase.getDriver;

public class AllureScreenShooter extends ExitCodeListener {
    public static boolean captureSuccessfulTests = true;

    @Override
    public void onTestFailure(ITestResult result) {
        super.onTestFailure(result);
        takeScreenshot();
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        super.onTestSuccess(result);
        /*if (captureSuccessfulTests) {*/
            takeScreenshot();
        //}
    }

    @Attachment(value = "Screenshot", type = "image/png", fileExtension = ".png")
    public static byte[] takeScreenshot() {
        return getScreenshotBytes();
    }

    public static byte[] getScreenshotBytes() {
        return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
    }
}

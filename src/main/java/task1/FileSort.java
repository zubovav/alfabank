package task1;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * У тебя есть текстовый файл, в котором есть значения от 0 до 20 в произвольном порядке (перемешаны). Значения указаны через запятую.
 * 1. Распарси файл, отфильтруй значения по возрастанию и выведи на консоль.
 * 2. Распарси файл, отфильтруй значения по убыванию и выведи на консоль.
 */
public class FileSort {

    public static void main(String[] args)  {
        descSort(System.getProperty("user.dir") + "\\src\\main\\resources\\task1Files\\1.txt");
        //ascSort(System.getProperty("user.dir") + "\\src\\main\\resources\\task1Files\\1.txt");
    }

    /**
     * Метод парсит файл, отфильтровывает значения по убыванию и выводит на консоль (диапазолн числе 0-20)
     * @param filePath Путь до файла. Afqkf
     */
    public static void descSort(String filePath)  {
        LinkedList<Integer> list = numbersList(filePath); // Получаем список чисел из файла
        // Сортирует список чисел по убыванию
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 > o2 ? -1 : o1.equals(o2) ? 0 : 1;
            }
        });
        //Если числа в файле меньше 0 или больше 20, выводим на консоль ошибку
        if(list.get(0) < 0 || list.get(list.size()-1) > 20){
            System.out.println("Error : Вышли за диапазон чисел 0-20");
            return;
        }
        // Выводим на консоль отсоритрованный по убыванию список чисел
        for (Integer aList : list) {
            System.out.print(aList + " ");
        }
    }

    /**
     * Метод парсит файл, отфильтровывает значения по возрастанию и выводит на консоль
     * @param filePath Путь до файла
     */
    public static void ascSort(String filePath)  {
        LinkedList<Integer> list = numbersList(filePath);// Получаем список чисел из файла
        // Сортирует список чисел по возрастанию
        list.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 < o2 ? -1 : o1.equals(o2) ? 0 : 1;
            }
        });
        //Если числа в файле меньше 0 или больше 20, выводим на консоль ошибку
        if(list.get(0) < 0 || list.get(list.size()-1) > 20){
            System.out.println("Error : Вышли за диапазон чисел 0-20");
            return;
        }
        // Выводим на консоль отсоритрованный по возрастанию список чисел
        for (Integer aList : list) {
            System.out.print(aList + " ");
        }
    }

    /**
     *
     * @param path Путь до файла
     * @return Список чисел из файла
     */
    private static LinkedList<Integer> numbersList(String path) {
        String text = readFile(path,StandardCharsets.UTF_8); // Получаем текст из файла в виде строки
        LinkedList<Integer> numberList = new LinkedList<>(); // Объявляем связный список
        String[] numbers = text.split(","); // В строковой массив записываем все числа
        for (String number : numbers) {
            numberList.add(Integer.valueOf(number));
        } // В список записали все числа
        return numberList;
    }

    /**
     * Читаем файл и возвращаем весь текст из файла в виде строки
     * @param path путь до файла
     * @param encoding кодирвка
     * @return весь текст из файла в виде строки
     * @throws IOException
     */
    private static String readFile(String path, Charset encoding)
    {
        byte[] str = new byte[0];
        try {
            str = Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(str, encoding);
    }
}
